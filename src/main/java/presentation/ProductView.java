package presentation;

import model.Product;

import java.awt.*;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Clasa ProductView contine implementarea ferestrei care se ocupa cu operatiile cu produse.
 */

public class ProductView {
    public JLabel l1 = new JLabel("Operation with products");
    public JLabel l2 = new JLabel("ID: ");
    public JLabel l3 = new JLabel("Name: ");
    public JLabel l4 = new JLabel("Quantity: ");
    public JTextArea tf1 = new JTextArea(1, 10);
    public JTextArea tf2 = new JTextArea(1, 10);
    public JTextArea tf3 = new JTextArea(1, 10);
    public JButton b1 = new JButton("Add product");
    public JButton b2 = new JButton("Edit product");
    public JButton b3 = new JButton("Delete product");
    public JButton b4 = new JButton("View products");
    private DefaultTableModel model = new DefaultTableModel();
    private JTable t = new JTable(model);

    public ProductView() {
        JFrame frame = new JFrame("");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(250, 400);

        JPanel panel = new JPanel();
        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JPanel panel3 = new JPanel();
        JPanel panel4 = new JPanel();
        JPanel panel5 = new JPanel();
        JPanel panel6 = new JPanel();
        JPanel panel7 = new JPanel();
        t.setBounds(30, 40, 200, 300);
        JScrollPane sp = new JScrollPane(t);

        FlowLayout flLayout = new FlowLayout();
        GridLayout grLayout = new GridLayout(0, 1, 0, 1);

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel1.setLayout(flLayout);
        panel2.setLayout(grLayout);
        panel3.setLayout(grLayout);
        panel4.setLayout(grLayout);
        panel5.setLayout(grLayout);
        panel6.setLayout(flLayout);
        panel7.setLayout(flLayout);

        panel.add(Box.createRigidArea(new Dimension(0, 5)));

        panel1.add(l1);
        panel2.add(l2);
        panel2.add(l3);
        panel2.add(l4);
        panel3.add(tf1);
        panel3.add(tf2);
        panel3.add(tf3);
        panel6.add(panel2);
        panel6.add(panel3);

        panel4.add(b1);
        panel4.add(b2);
        panel5.add(b3);
        panel5.add(b4);
        panel7.add(panel4);
        panel7.add(panel5);

        panel.add(panel1);
        panel.add(Box.createRigidArea(new Dimension(0, 5)));
        panel.add(panel6);
        panel.add(Box.createRigidArea(new Dimension(0, 5)));
        panel.add(panel7);
        panel.add(Box.createRigidArea(new Dimension(0, 5)));
        panel.add(sp);
        panel.add( Box.createRigidArea(new Dimension(0,5)) );

        frame.setContentPane(panel);
        frame.setVisible(true);
    }
    public String getFirst() {
        return tf1.getText();
    }
    public String getSecond() {
        return tf2.getText();
    }
    public String getThird() {
        return tf3.getText();
    }

    public void show(ArrayList<Product> product){
        boolean s = true;
        model.setColumnCount(0);
        model.setRowCount(0);
        for(Product i : product) {
            Vector a = new Vector();
            for (Field field : i.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                Object value;
                try {
                    value = field.get(i);
                    a.add(value);
                    if(s) {
                        model.addColumn(field.getName());
                    }
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            s = false;
            model.addRow(a);
        }
    }

    void addB1Listener(ActionListener add) {
        b1.addActionListener(add);
    }
    void addB2Listener(ActionListener edit) {
        b2.addActionListener(edit);
    }
    void addB3Listener(ActionListener del) {
        b3.addActionListener(del);
    }
    void addB4Listener(ActionListener view) {
        b4.addActionListener(view);
    }
}
