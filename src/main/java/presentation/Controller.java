package presentation;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import model.Client;
import model.Order;
import model.Product;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Clasa Controller primeste informatiile introduse in interfata utilizator
 * si le transmite claselor din pachetul bll.
 */

public class Controller {
    private ClientView c;
    private ProductView p;
    private OrderView o;
    private ClientBLL cbl;
    private ProductBLL pbl;
    private OrderBLL obl;

    public Controller(ClientView c, ProductView p, OrderView o){
        this.c = c;
        this.p = p;
        this.o = o;

        cbl = new ClientBLL();
        pbl = new ProductBLL();
        obl = new OrderBLL();

        c.addB1Listener(new AddClB1Listener());
        c.addB2Listener(new AddClB2Listener());
        c.addB3Listener(new AddClB3Listener());
        c.addB4Listener(new AddClB4Listener());

        p.addB1Listener(new AddPrB1Listener());
        p.addB2Listener(new AddPrB2Listener());
        p.addB3Listener(new AddPrB3Listener());
        p.addB4Listener(new AddPrB4Listener());

        o.addB1Listener(new AddOrB1Listener());
    }

    class AddClB1Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String in1, in2, in3;
            in1 = c.getSecond();
            in2 = c.getThird();
            in3 = c.getFourth();
            Client client = new Client(in1,in2,in3);
            cbl.insertClient(client);
        }
    }
    class AddClB2Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String in2, in3, in4;
            int in1;
            try{
                in1 = Integer.parseInt(c.getFirst());
            } catch (NumberFormatException s) {
                return;
            }
            in2 = c.getSecond();
            in3 = c.getThird();
            in4 = c.getFourth();
            cbl.updateClient(in1,in2,in3,in4);
        }
    }
    class AddClB3Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int in1;
            try{
                in1 = Integer.parseInt(c.getFirst());
            } catch (NumberFormatException s) {
                return;
            }
            cbl.deleteClient(in1);
        }
    }
    class AddClB4Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            c.show(cbl.findAllClients());
        }
    }


    class AddPrB1Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String in1;
            int in2 = 0;
            in1 = p.getSecond();
            try{
                in2 = Integer.parseInt(p.getThird());
            } catch (NumberFormatException s) {
                return;
            }
            Product product = new Product(in1,in2);
            pbl.insertProduct(product);
        }
    }
    class AddPrB2Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String in2;
            int in1, in3;
            try{
                in1 = Integer.parseInt(p.getFirst());
            } catch (NumberFormatException s) {
                return;
            }
            in2 = p.getSecond();
            try{
                in3 = Integer.parseInt(p.getThird());
            } catch (NumberFormatException s) {
                in3 = -1;
            }
            pbl.updateProduct(in1,in2,in3);
        }
    }
    class AddPrB3Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int in1;
            try{
                in1 = Integer.parseInt(p.getFirst());
            } catch (NumberFormatException s) {
                return;
            }
            pbl.deleteProduct(in1);
        }
    }
    class AddPrB4Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            p.show(pbl.findAllProducts());
        }
    }


    class AddOrB1Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int in1, in2, in3;
            try{
                in1 = Integer.parseInt(o.getFirst());
            } catch (NumberFormatException s) {
                return;
            }
            try{
                in2 = Integer.parseInt(o.getSecond());
            } catch (NumberFormatException s) {
                return;
            }
            try{
                in3 = Integer.parseInt(o.getThird());
            } catch (NumberFormatException s) {
                return;
            }
            Order order = new Order(in1,in2,in3);
            int k = obl.insertOrder(order);
            if(k == -2)
                o.showErr2();
            else if(k == -1)
                o.showErr1();
            else {
                o.showFin();
                Client client = cbl.findClientById(in1);
                Product product = pbl.findProductById(in2);
                o.genBill(client.getName(), product.getName(), in3);
            }
        }
    }
}
