package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Clasa OrderView contine implementarea ferestrei prin care se poate crea o comanda.
 */

public class OrderView {
    public JLabel l1 = new JLabel("Create order");
    public JLabel l2 = new JLabel("Client ID: ");
    public JLabel l3 = new JLabel("Product ID: ");
    public JLabel l4 = new JLabel("Quantity: ");
    public JTextArea tf1 = new JTextArea(1, 10);
    public JTextArea tf2 = new JTextArea(1, 10);
    public JTextArea tf3 = new JTextArea(1, 10);
    public JTextArea tf4 = new JTextArea(1, 10);
    public JButton b1 = new JButton("Order");

    public OrderView() {
        JFrame frame = new JFrame("");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(250, 250);

        JPanel panel = new JPanel();
        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JPanel panel3 = new JPanel();
        JPanel panel4 = new JPanel();
        JPanel panel5 = new JPanel();
        JPanel panel6 = new JPanel();

        FlowLayout flLayout = new FlowLayout();
        GridLayout grLayout = new GridLayout(0, 1, 0, 1);

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel1.setLayout(flLayout);
        panel2.setLayout(grLayout);
        panel3.setLayout(grLayout);
        panel4.setLayout(flLayout);
        panel5.setLayout(flLayout);
        panel6.setLayout(flLayout);

        panel.add(Box.createRigidArea(new Dimension(0, 5)));

        panel1.add(l1);
        panel2.add(l2);
        panel2.add(l3);
        panel2.add(l4);
        panel3.add(tf1);
        panel3.add(tf2);
        panel3.add(tf3);
        panel5.add(panel2);
        panel5.add(panel3);

        panel4.add(b1);
        panel6.add(tf4);

        panel.add(panel1);
        panel.add(Box.createRigidArea(new Dimension(0, 5)));
        panel.add(panel5);
        panel.add(Box.createRigidArea(new Dimension(0, 5)));
        panel.add(panel4);
        panel.add(Box.createRigidArea(new Dimension(0, 5)));
        panel.add(panel6);
        panel.add(Box.createRigidArea(new Dimension(0, 5)));

        frame.setContentPane(panel);
        frame.setVisible(true);
    }
    public String getFirst() {
        return tf1.getText();
    }
    public String getSecond() {
        return tf2.getText();
    }
    public String getThird() {
        return tf3.getText();
    }

    public void genBill(String client, String product, int quantity){
        try {
            File file = new File("bill.txt");
            file.createNewFile();
            FileWriter f = new FileWriter("bill.txt",false);
            f.write("Client " + client + " has ordered " + quantity + " of the product " + product + ".\n");
            f.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
        }
    }

    public void showFin(){
        tf4.setText("Order processed");
    }

    public void showErr1(){
        tf4.setText("Nonexistent client or product");
    }

    public void showErr2(){
        tf4.setText("Under stock");
    }

    void addB1Listener(ActionListener add) {
        b1.addActionListener(add);
    }
}
