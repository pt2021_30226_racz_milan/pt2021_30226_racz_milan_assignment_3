package start;

import presentation.ClientView;
import presentation.Controller;
import presentation.OrderView;
import presentation.ProductView;

public class Main {
    public static void main(String[] args){
        ClientView c = new ClientView();
        ProductView p = new ProductView();
        OrderView o = new OrderView();
        Controller ctr = new Controller(c, p, o);
    }
}
