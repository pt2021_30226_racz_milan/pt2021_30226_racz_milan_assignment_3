package dao;

import model.Product;

/**
 * Clasa ProductDA extinde clasa AbstractDA si contine metode care transmit informatiile
 * necesare metodelor de insert si update pentru generarea de query.
 */

public class ProductDA extends AbstractDA<Product> {

    public int ins(Product product){
        String names = "name, quantity";
        String values = "'" + product.getName() + "', " + product.getQuantity();
        return insert(names,values);
    }

    public int upd(Product product){
        String names = "name = " + "'" + product.getName() + "',quantity = " + product.getQuantity();
        return update(names,product.getId());
    }
}
