package dao;

import model.Order;


/**
 * Clasa OrderDA extinde clasa AbstractDA si conține o metoda care transmite informatiile
 * necesare metodei de insert pentru generarea de query.
 */

public class OrderDA extends AbstractDA<Order> {

    public int ins(Order order) {
        String names = "clientId,productId,quantity";
        String values = order.getClientId() + "," + order.getProductId() + "," + order.getQuantity();
        return insert(names,values);
    }
}
