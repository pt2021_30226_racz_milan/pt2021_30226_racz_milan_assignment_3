package dao;

import model.Client;

/**
 * Clasa ClientDA extinde clasa AbstractDA si contine metode care transmit informatiile
 * necesare metodelor de insert si update pentru generarea de query.
 */

public class ClientDA extends AbstractDA<Client>{

    public int ins(Client client){
        String names = "name,address,email";
        String values = "'" + client.getName() + "','" + client.getAddress() + "','" + client.getEmail() + "'";
        return insert(names,values);
    }

    public int upd(Client client){
        String names = "name = '" + client.getName() + "',address = '" + client.getAddress() + "',email = '" + client.getEmail() + "'";
        return update(names,client.getId());
    }
}

