package bll;

import dao.ProductDA;
import model.Product;

import java.util.ArrayList;
import java.util.NoSuchElementException;

/**
 * Clasa ProductBLL este responsabila de verificarea datelor introduse in ProductView si in
 * cazul in care acestea sunt acceptabile, se realizeaza operația dorita.
 */

public class ProductBLL {

    public Product findProductById(int id) {
        ProductDA da = new ProductDA();
        Product p = da.findById(id);
        if (p == null) {
            throw new NoSuchElementException("The product with id = " + id + " was not found!");
        }
        return p;
    }

    public ArrayList<Product> findAllProducts() {
        ProductDA da = new ProductDA();
        return da.findAll();
    }

    public int insertProduct(Product product) {
        ProductDA da = new ProductDA();
        return da.ins(product);
    }

    public int updateProduct(int id, String name, int quantity) {
        Product p;
        try {
            p = findProductById(id);
        }catch (NoSuchElementException e){
            return -1;
        }
        if(name.equals(""))
            name = p.getName();
        if(quantity == -1)
            quantity = p.getQuantity();
        ProductDA da = new ProductDA();
        return da.upd(new Product(id,name,quantity));
    }

    public int deleteProduct(int id) {
        Product p;
        try {
            p = findProductById(id);
        }catch (NoSuchElementException e){
            return -1;
        }
        ProductDA da = new ProductDA();
        return da.delete(id);
    }
}
