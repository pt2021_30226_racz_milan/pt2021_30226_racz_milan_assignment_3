package bll;

import dao.ClientDA;
import model.Client;

import java.util.ArrayList;
import java.util.NoSuchElementException;

/**
 * Clasa ClientBLL este responsabila de verificarea datelor introduse în ClientView si in
 * cazul in care acestea sunt acceptabile, se realizeaza operația dorita.
 */

public class ClientBLL {

    public Client findClientById(int id) {
        ClientDA da = new ClientDA();
        Client c = da.findById(id);
        if (c == null) {
            throw new NoSuchElementException("The client with id = " + id + " was not found!");
        }
        return c;
    }
    public ArrayList<Client> findAllClients() {
        ClientDA da = new ClientDA();
        return da.findAll();
    }

    public int insertClient(Client client) {
        ClientDA da = new ClientDA();
        return da.ins(client);
    }

    public int updateClient(int id, String name, String address, String email) {
        Client c;
        try {
            c = findClientById(id);
        }catch (NoSuchElementException e){
            return -1;
        }
        if(name.equals(""))
            name = c.getName();
        if(address.equals(""))
            address = c.getAddress();
        if(email.equals(""))
            email = c.getEmail();
        ClientDA da = new ClientDA();
        return da.upd(new Client(id,name,address,email));
    }

    public int deleteClient(int id) {
        Client c;
        try {
            c = findClientById(id);
        }catch (NoSuchElementException e){
            return -1;
        }
        ClientDA da = new ClientDA();
        return da.delete(id);
    }
}

