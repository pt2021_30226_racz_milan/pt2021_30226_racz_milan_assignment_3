package bll;

import dao.OrderDA;
import model.Order;
import model.Product;

import java.util.NoSuchElementException;

/**
 * Clasa OrderBLL este responsabila de verificarea datelor introduse in OrderView si in
 * cazul in care acestea sunt acceptabile, se realizeaza operația dorita.
 */

public class OrderBLL {

    public Order findOrderById(int id) {
        OrderDA da = new OrderDA();
        Order o = da.findById(id);
        if (o == null) {
            throw new NoSuchElementException("The order with id = " + id + " was not found!");
        }
        return o;
    }

    public int insertOrder(Order order) {
        ClientBLL cl = new ClientBLL();
        try {
            cl.findClientById(order.getClientId());
        }catch (NoSuchElementException e){
            return -1;
        }
        ProductBLL pr = new ProductBLL();
        Product p;
        try {
            p = pr.findProductById(order.getProductId());
        }catch (NoSuchElementException e){
            return -1;
        }
        if(p.getQuantity() >= order.getQuantity()){
            pr.updateProduct(p.getId(),"",p.getQuantity() - order.getQuantity());
            OrderDA da = new OrderDA();
            return da.ins(order);
        }
        else
            return -2;
    }

    public int deleteOrder(int id) {
        Order o;
        try {
            o = findOrderById(id);
        }catch (NoSuchElementException e){
            return -1;
        }
        OrderDA da = new OrderDA();
        return da.delete(id);
    }
}
