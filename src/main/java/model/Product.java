package model;

/**
 * In clasa Product am declarat variabile in care se vor stoca informatiile necesare despre produse,
 * extrase din baza de date sau de introdus in aceasta.
 */

public class Product {
    private int id;
    private String name;
    private int quantity;

    public Product(){
    }

    public Product(int id, String name, int quantity){
        this.id = id;
        this.name = name;
        this.quantity = quantity;
    }

    public Product(String name, int quantity){
        this.name = name;
        this.quantity = quantity;
    }

    public void setId(int id){
        this.id = id;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setQuantity(int quantity){
        this.quantity = quantity;
    }

    public int getId(){
        return id;
    }
    public String getName(){
        return name;
    }
    public int getQuantity(){
        return quantity;
    }
}
