package model;

/**
 * In clasa Order am declarat variabile in care se vor stoca informatiile necesare despre comenzi,
 * extrase din baza de date sau de introdus in aceasta.
 */

public class Order {
    private int orderId;
    private int clientId;
    private int productId;
    private int quantity;

    public Order(){
    }

    public Order(int orderId, int clientId, int productId, int quantity){
        this.orderId = orderId;
        this.clientId = clientId;
        this.productId = productId;
        this.quantity = quantity;
    }
    public Order(int clientId, int productId, int quantity){
        this.clientId = clientId;
        this.productId = productId;
        this.quantity = quantity;
    }

    public void setOrderId(int orderId){
        this.orderId = orderId;
    }
    public void setClientId(int clientId){
        this.clientId = clientId;
    }
    public void setProductId(int productId){
        this.productId = productId;
    }

    public int getOrderId(){
        return orderId;
    }
    public int getClientId(){
        return clientId;
    }
    public int getProductId(){
        return productId;
    }
    public int getQuantity(){
        return quantity;
    }
}
